<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'modiko' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[d7:>CiN^--*>=-6bkJ2qX=vw0+J$[kRUHXJjNr-s/iwEgWY;6wl}Ax*P!s%is68' );
define( 'SECURE_AUTH_KEY',  '~><C8Oc}Jo`6vCjL!HGh==dvM*XW?|/|X^`[Tmr3*H@-vSqee!^f<DY$ku:%K>Go' );
define( 'LOGGED_IN_KEY',    'Q8hJ&E~* /$+/I1)Odftju?_3$QX%/sALa^NUmZJaomqg/7wFM6 ]l Jn|<kW>[E' );
define( 'NONCE_KEY',        '8c,lgbH,&?iS~0w}$cnW]nu6)6R1F9/dHd%~A6fw|gG7j,i+hULNDH}w&P1*x+w}' );
define( 'AUTH_SALT',        '**<@rtG/k<~p!3grmqT(f?5aE37mQcFT3`hl#ntPbb3C3}(Tk<i&<AH_sIu5izUV' );
define( 'SECURE_AUTH_SALT', '#{>A;!uEjBmi!C 5tNbHxMxOmsf*oh[PRpAKBaW98w4|]@QlR=LaSjq+v8>a;cUt' );
define( 'LOGGED_IN_SALT',   'zf0VHiiEhPyv3[>3>wokd_W_^*]_-f]S5kX5F!{<~oeOeXi4N.q;5|t!;q<G;YAv' );
define( 'NONCE_SALT',       'bV*4Ym_J=_-C4%(iIhwq-2Vsm)Ht )u-V@?;NF:%,p/R?AS.T_Jda}t2$|m2 9Tc' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
